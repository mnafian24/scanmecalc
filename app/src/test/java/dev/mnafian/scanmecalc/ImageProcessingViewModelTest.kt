package dev.mnafian.scanmecalc

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import dev.mnafian.scanmecalc.ui.viewmodel.ImageProcessingViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class ImageProcessingViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var observer: Observer<String?>

    private lateinit var imageProcessingViewModel: ImageProcessingViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        imageProcessingViewModel = ImageProcessingViewModel()
        imageProcessingViewModel.textResult.observeForever(observer)
    }

    @Test
    fun testCalculateFromExpression_ValidExpression() {
        val expression = "5*3"
        val expectedResult = "15.0"

        imageProcessingViewModel.calculateFromExpression(expression)

        // Verify that the observer was triggered and received the expected result
        `when`(observer.onChanged(expectedResult)).then {
            assert(imageProcessingViewModel.textResult.value == expectedResult)
        }
    }

    @Test
    fun testCalculateFromExpression_ComplexExpressionShouldGetFirstArgument() {
        val expression = "5*3(abcde=)"
        val expectedResult = "15.0"

        imageProcessingViewModel.calculateFromExpression(expression)

        // Verify that the observer was triggered and received the expected result
        `when`(observer.onChanged(expectedResult)).then {
            assert(imageProcessingViewModel.textResult.value == expectedResult)
        }
    }

    @Test
    fun testCalculateFromExpression_InvalidExpression() {
        val expression = "%%%%8990+10"

        imageProcessingViewModel.calculateFromExpression(expression)

        // Verify that the observer was triggered and received a null result
        `when`(observer.onChanged(null)).then {
            assert(imageProcessingViewModel.textResult.value == null)
        }
    }
}
