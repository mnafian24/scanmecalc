package dev.mnafian.scanmecalc.model

import android.graphics.Bitmap
import android.net.Uri

data class CameraResult(val imageUri: Uri?, val previewBitmap: Bitmap?)
