package dev.mnafian.scanmecalc.data

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import dev.mnafian.scanmecalc.textdetector.BitmapUtils

class ImageRepository {

    var tempImageUri: Uri? = null

    fun startCameraIntentForResult(activity: Activity) {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(activity.packageManager) != null) {
            val values = ContentValues()
            values.put(MediaStore.Images.Media.TITLE, "New Picture")
            values.put(MediaStore.Images.Media.DESCRIPTION, "From Camera")
            val imageUri = activity.contentResolver.insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values
            )
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)

            activity.startActivityForResult(
                takePictureIntent,
                REQUEST_IMAGE_CAPTURE
            )
            tempImageUri = imageUri
        }
    }

    fun startChooseImageIntentForResult(activity: Activity) {
        // Implement the choose image intent logic here
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        activity.startActivityForResult(
            Intent.createChooser(intent, "Select Picture"),
            REQUEST_CHOOSE_IMAGE
        )
    }

    fun loadPreviewBitmap(context: Context, imageUri: Uri?): Bitmap? {
        if (imageUri == null) {
            return null
        }
        return BitmapUtils.getBitmapFromContentUri(context.contentResolver, imageUri)
    }

    companion object {
        const val REQUEST_IMAGE_CAPTURE = 1001
        const val REQUEST_CHOOSE_IMAGE = 1002
    }
}
