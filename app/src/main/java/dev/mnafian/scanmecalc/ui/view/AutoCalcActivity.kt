package dev.mnafian.scanmecalc.ui.view

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.common.annotation.KeepName
import dev.mnafian.scanmecalc.BuildConfig
import dev.mnafian.scanmecalc.R
import dev.mnafian.scanmecalc.data.ImageRepository
import dev.mnafian.scanmecalc.textdetector.GraphicOverlay
import dev.mnafian.scanmecalc.ui.viewmodel.ImageProcessingViewModel

@KeepName
class AutoCalcActivity : AppCompatActivity(), ActivityCompat.OnRequestPermissionsResultCallback {
    private var preview: ImageView? = null
    private var resultCalc: TextView? = null
    private var graphicOverlay: GraphicOverlay? = null
    private var selectedSize: String? = SIZE_SCREEN
    private var isLandScape = false

    // Max width (portrait mode)
    private var imageMaxWidth = 0

    // Max height (portrait mode)
    private var imageMaxHeight = 0

    private lateinit var viewModel: ImageProcessingViewModel
    private lateinit var imageRepository: ImageRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_calc)

        // Initialize ViewModel and Repository
        viewModel = ViewModelProvider(this)[ImageProcessingViewModel::class.java]
        imageRepository = ImageRepository()

        // Set up the UI and other event listeners
        if (allRuntimePermissionsGranted()) {
            setupUI()
        } else {
            getRuntimePermissions()
        }
    }

    private fun allRuntimePermissionsGranted(): Boolean {
        for (permission in REQUIRED_RUNTIME_PERMISSIONS) {
            permission.let {
                if (!isPermissionGranted(this, it)) {
                    return false
                }
            }
        }
        return true
    }

    private fun getRuntimePermissions() {
        val permissionsToRequest = ArrayList<String>()
        for (permission in REQUIRED_RUNTIME_PERMISSIONS) {
            permission.let {
                if (!isPermissionGranted(this, it)) {
                    permissionsToRequest.add(permission)
                }
            }
        }

        if (permissionsToRequest.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                permissionsToRequest.toTypedArray(),
                PERMISSION_REQUESTS
            )
        }
    }

    private fun requestCameraPermissionAndStartCamera() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            startCameraIntentForResult()
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                PERMISSION_REQUESTS
            )
        }
    }

    private fun requestStoragePermissionAndStartChooseImage() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            startChooseImageIntentForResult()
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PERMISSION_REQUESTS
            )
        }
    }

    private fun isPermissionGranted(context: Context, permission: String): Boolean {
        if (ContextCompat.checkSelfPermission(
                context,
                permission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun setupUI() {
        preview = findViewById(R.id.preview)
        resultCalc = findViewById(R.id.result_text)
        graphicOverlay = findViewById(R.id.graphic_overlay)

        isLandScape = resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE

        val rootView = findViewById<View>(R.id.root)
        rootView.viewTreeObserver.addOnGlobalLayoutListener(
            object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    rootView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    imageMaxWidth = rootView.width
                    imageMaxHeight = rootView.height - findViewById<View>(R.id.control).height
                    if (SIZE_SCREEN == selectedSize) {
                        tryReloadAndDetectInImage()
                    }
                }
            }
        )

        findViewById<View>(R.id.select_image_button).setOnClickListener { view: View ->
            val popup = PopupMenu(this@AutoCalcActivity, view)
            val inflater = popup.menuInflater
            inflater.inflate(R.menu.camera_button_menu, popup.menu)
            val showGallery = resources.getBoolean(R.bool.showGallery)
            val showCamera = resources.getBoolean(R.bool.showCamera)

            popup.menu.findItem(R.id.select_images_from_local)?.isVisible = showGallery && showCamera == false
            popup.menu.findItem(R.id.take_photo_using_camera)?.isVisible = showCamera && showGallery == false

            popup.setOnMenuItemClickListener { menuItem: MenuItem ->
                val itemId = menuItem.itemId
                if (itemId == R.id.select_images_from_local) {
                    requestStoragePermissionAndStartChooseImage()
                    return@setOnMenuItemClickListener true
                } else if (itemId == R.id.take_photo_using_camera) {
                    requestCameraPermissionAndStartCamera()
                    return@setOnMenuItemClickListener true
                }
                false
            }

            popup.show()
        }

        viewModel.textResult.observe(this) { text ->
            resultCalc?.text = text
        }
    }

    public override fun onResume() {
        super.onResume()
        viewModel.createImageProcessor(this)
        tryReloadAndDetectInImage()
    }

    public override fun onPause() {
        super.onPause()
        viewModel.stopImageProcessor()
    }

    public override fun onDestroy() {
        super.onDestroy()
        viewModel.stopImageProcessor()
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    private fun startCameraIntentForResult() {
        // Clean up last time's image
        preview?.setImageBitmap(null)
        imageRepository.startCameraIntentForResult(this)
    }

    private fun startChooseImageIntentForResult() {
        preview?.setImageBitmap(null)
        imageRepository.startChooseImageIntentForResult(this)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ImageRepository.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            // Notify ViewModel to process the captured image
            viewModel.processChosenImage(this, imageRepository.tempImageUri)
            tryReloadAndDetectInImage()
        } else if (requestCode == ImageRepository.REQUEST_CHOOSE_IMAGE && resultCode == Activity.RESULT_OK) {
            // Notify ViewModel to process the chosen image
            viewModel.processChosenImage(this, data?.data)
            tryReloadAndDetectInImage()
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun tryReloadAndDetectInImage() {
        if (SIZE_SCREEN == selectedSize && imageMaxWidth == 0) {
            // UI layout has not finished yet, will reload once it's ready.
            return
        }
        viewModel.imageBitmap.observe(this) { imageBitmap ->
            if (imageBitmap != null) {
                graphicOverlay?.clear()
                preview?.setImageBitmap(imageBitmap)

                // Clear the overlay first
                if (viewModel.imageProcessor.value != null) {
                    graphicOverlay?.setImageSourceInfo(
                        imageBitmap.width,
                        imageBitmap.height,
                        /* isFlipped= */ false
                    )
                    viewModel.imageProcessor.value?.processBitmap(imageBitmap, graphicOverlay)
                } else {
                    Log.e(
                        "Error",
                        "Null imageProcessor, please check adb logs for imageProcessor creation error"
                    )
                }

            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_REQUESTS -> {
                if (allPermissionsGranted(grantResults)) {
                    setupUI()
                } else {
                    // Handle permission denied
                }
            }
            // Handle other permission requests if needed
        }
    }

    private fun allPermissionsGranted(grantResults: IntArray): Boolean {
        for (result in grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    companion object {

        private const val SIZE_SCREEN = "w:screen" // Match screen width

        private const val PERMISSION_REQUESTS = 1

        private val REQUIRED_RUNTIME_PERMISSIONS =
            arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
    }
}
