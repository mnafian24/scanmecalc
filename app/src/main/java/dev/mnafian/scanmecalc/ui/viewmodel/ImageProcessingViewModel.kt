package dev.mnafian.scanmecalc.ui.viewmodel

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.mlkit.vision.text.Text
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import dev.mnafian.scanmecalc.data.ImageRepository
import dev.mnafian.scanmecalc.textdetector.TextRecognitionListener
import dev.mnafian.scanmecalc.textdetector.TextRecognitionProcessor
import dev.mnafian.scanmecalc.textdetector.VisionImageProcessor

class ImageProcessingViewModel : ViewModel(),
    TextRecognitionListener {
    private val imageRepository = ImageRepository()

    private val _imageBitmap = MutableLiveData<Bitmap>()
    val imageBitmap: LiveData<Bitmap>
        get() = _imageBitmap

    private var _imageProcessor = MutableLiveData<VisionImageProcessor>()
    val imageProcessor: LiveData<VisionImageProcessor>
        get() = _imageProcessor

    private var _textResult = MutableLiveData<String>()
    val textResult: LiveData<String>
        get() = _textResult

    fun processChosenImage(context: Context, imageUri: Uri?) {
        _imageBitmap.value = imageRepository.loadPreviewBitmap(context, imageUri)
    }

    fun createImageProcessor(context: Context) {
        try {
            val textRecognitionProcessor =
                TextRecognitionProcessor(context, TextRecognizerOptions.Builder().build(), this)
            _imageProcessor.value = textRecognitionProcessor
        } catch (e: Exception) {
            Toast.makeText(
                context,
                "Can not create image processor: " + e.message,
                Toast.LENGTH_LONG
            )
                .show()
        }
    }

    fun stopImageProcessor() {
        imageProcessor.run { value?.stop() }
    }

    fun calculateFromExpression(expression: String): Double? {
        val pattern = Regex("([0-9.]+)([+\\-xX*:/])([0-9.]+)")
        val matchResult = pattern.find(expression)

        matchResult?.groupValues?.let { groups ->
            val firstNumber = groups[1].toDoubleOrNull()
            val operator = groups[2]
            val secondNumber = groups[3].toDoubleOrNull()

            if (firstNumber != null && secondNumber != null) {
                return when (operator) {
                    "+" -> firstNumber + secondNumber
                    "-" -> firstNumber - secondNumber
                    "x", "X", "*" -> firstNumber * secondNumber
                    "/", ":" -> if (secondNumber != 0.0) firstNumber / secondNumber else null
                    else -> null
                }
            }
        }

        return null // Invalid expression or unable to perform calculation
    }

    override fun onTextRecognitionResult(textResult: List<Text.TextBlock>) {
        val firstScanResult = textResult.first().text.replace(" ", "").trim()
        val result = calculateFromExpression(firstScanResult)
            ?: "Unable to detect complex multiple text, please make sure target image is single line of expression only"
        _textResult.postValue("Result = $result")
    }
}
