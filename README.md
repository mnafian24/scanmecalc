# ScanMeCalc App

ScanMeCalc is a simple Android application that allows users to perform calculations by scanning math expressions from images. It utilizes the power of on-device machine learning to recognize and evaluate math expressions in images.

## Features

- Scan math expressions from images using on-device machine learning.
- Perform calculations on recognized math expressions.
- Utilizes a clean and simple MVVM architecture for better code organization and maintainability.

## Architecture

The app follows a simple MVVM (Model-View-ViewModel) architecture for separation of concerns and better code organization:

- **Model**: Represents the data and business logic. It includes classes for text recognition, expression evaluation, and data storage.
- **View**: Displays the UI to the user. It consists of activities, fragments, and layout files.
- **ViewModel**: Acts as a bridge between the View and the Model. It holds the presentation logic and exposes data to the View.

## Unit Tests

The ViewModel of the ScanMeCalc app is unit-tested to ensure its correctness and reliability. The tests cover various scenarios, including expression recognition, calculation, and user interactions.

To run the unit tests, navigate to the `app/src/test/java` directory and run the relevant test classes.

## Flavors and Build Types

ScanMeCalc supports different flavors and build types to provide customized versions of the app. Four flavors are available, each with specific features:

- `red-filesystem`: Supports picking pictures from the filesystem only.
- `red-built-in-camera`: Supports using the built-in camera only.
- `green-filesystem`: Supports picking pictures from the filesystem only.
- `green-built-in-camera`: Supports using the built-in camera only.

To build a specific flavor, use the following Gradle command:

`./gradlew assembleFlavorType`


For example, to build the `red-filesystem` flavor, use:

`/gradlew assembleRedFilesystem`


## Screenshots

| ![Green Built In Camera](https://i.postimg.cc/Ls1hg7Ls/Whats-App-Image-2023-08-15-at-12-03-35-AM.jpg) | ![Green Gallery only](https://i.postimg.cc/FsL6qVyL/Whats-App-Image-2023-08-15-at-12-06-13-AM.jpg) |
|:---:|:---:|
| *Green theme with built-in camera* | *Green theme with gallery only* |

| ![Red Built In Camera](https://i.postimg.cc/bYT0vDqB/Whats-App-Image-2023-08-15-at-12-10-02-AM.jpg) | ![Red Gallery only](https://i.postimg.cc/W1P5yDwp/Whats-App-Image-2023-08-15-at-12-19-25-AM.jpg) |
|:---:|:---:|
| *Red theme with built-in camera* | *Red theme with gallery only* |


## Getting Started

To use the ScanMeCalc app, follow these steps:

1. Clone the repository to your local machine.
2. Open the project in Android Studio.
3. Build and run the app on an emulator or physical device.

## Contributing

Contributions to ScanMeCalc are welcome! If you find any issues or have suggestions for improvements, please feel free to submit a pull request.

## License

ScanMeCalc is released under the [MIT License](LICENSE).

